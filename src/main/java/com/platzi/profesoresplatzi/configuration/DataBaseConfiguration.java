package com.platzi.profesoresplatzi.configuration;

import java.util.Properties;

import javax.sql.DataSource;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

//Toda esta clase de configuracion proviene de un archivo de configuracion: hibernate.cfg.xml
@Configuration
@EnableTransactionManagement //Esto es para que todas las acciones en base de datos se hagan por medio de transacciones 
public class DataBaseConfiguration {
	
	@Bean/*Cada vez que venga la anotacion "@Configuration", va aver un Bean
	Y esto es para que spring sepa cuando crearlo y cuando destruirlo*/
	public LocalSessionFactoryBean sessionFactory(){
		//Aqui se puede utilizar new ya que es un objeto de configuracion, a quien se va a instanciar
		LocalSessionFactoryBean sessionFactoryBean = new LocalSessionFactoryBean();
		//Todos los datos de nuestra configuracion a la base de datos
		sessionFactoryBean.setDataSource(dataSource());//setDataSource: sera todos los datos de nuestra configuracion de nuestra base de datos
		sessionFactoryBean.setPackagesToScan("com.platzi.profesoresplatzi.model");//Este paquese estan todas las clases donde se mapearan todas nuestras tablas de la base de datos
		sessionFactoryBean.setHibernateProperties(hibernateProperties());//Viene del xml: name="hibernate.dialect"
		return sessionFactoryBean;
	}
	
	//Es un metodo que va a configurar los datos a la base de datos
	@Bean //El bean se pone para spring sepa que debe el iniciar el objeto y destruirlo cuando ya no este en uso
	public DataSource dataSource(){
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName("com.mysql.jdbc.Driver");
		dataSource.setUrl("jdbc:mysql://localhost:3306/platziprofesores");
		dataSource.setUsername("root");
		//dataSource.setPassword("");
		
		return dataSource;
	}
	
	/*Estos datos se obtuvieron de la base de datos
	Con el fin de eliminar poco a poco el xml de configuracion*/
	//No se le pone el bean ya que no se desea que se maneje su ciclo de vida para este metodo
	public Properties hibernateProperties(){
		Properties properties = new Properties();
		properties.put("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");
		properties.put("show_sql", "true");
		
		return properties;
	}
	
	@Bean
	@Autowired //para que sea un metodo persistido por hibernate
	public HibernateTransactionManager transactionManager(){
		HibernateTransactionManager hibernateTransactionManager = new HibernateTransactionManager();
		hibernateTransactionManager.setSessionFactory(sessionFactory().getObject());
		return hibernateTransactionManager;
	}
	
	
	
	
	
	
}
