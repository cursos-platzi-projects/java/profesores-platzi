package com.platzi.profesoresplatzi.dao;

import java.util.Iterator;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.platzi.profesoresplatzi.model.Teacher;
import com.platzi.profesoresplatzi.model.TeacherSocialMedia;


//Esta clase es usada para poder implementar los metodos y poder usarlos en otra clase que seria la clase main.
//Extendemos la clase PlatziSession para poder abrir la conexion
@Repository
@Transactional
public class TeacherDaoImpl extends AbstractSession implements TeacherDao {
	
	public void saveTeacher(Teacher teacher) {
		// TODO Auto-generated method stub
		getSession().persist(teacher);
	}

	public void deleteTeacherById(Long idTeacher) {
		// TODO Auto-generated method stub
		Teacher teacher = findById(idTeacher);
		if (teacher != null){
			//Para iterar todos los teachers que tienen social media
			//ejemplo: Su: facebook, twitter, youtube, etc.
			Iterator<TeacherSocialMedia> i = teacher.getTeacherSocialMedia().iterator();
			while(i.hasNext()){
				TeacherSocialMedia teacherSocialMedia = i.next();//Obtener el objeto y almacenarlo en teacherSocialMedia
				i.remove();//Para quitarlo de la lista
				getSession().delete(teacherSocialMedia);//Eliminamos el objeto teacherSocialMedia
			}
			//Asegurarnos de borrar toda la lista de TeacherSocialMedia
			teacher.getTeacherSocialMedia().clear();
			//Y con esto se elimina el objeto de teacher(Al final de eliminar sus social media, se eliminara al teacher)
			getSession().delete(teacher);
		}
	}

	public void updateTeacher(Teacher teacher) {
		// TODO Auto-generated method stub
		getSession().update(teacher);
	}

	public List<Teacher> findAllTeachers() {
		// TODO Auto-generated method stub
		return getSession().createQuery("from Teacher").list();
	}

	public Teacher findById(Long idTeacher) {
		// TODO Auto-generated method stub
		return (Teacher) getSession().get(Teacher.class, idTeacher);
	}

	public Teacher findByName(String name) {
		// TODO Auto-generated method stub
		return (Teacher) getSession().createQuery(
				"from Teacher where name = :name")
				.setParameter("name", name).uniqueResult();
	}

}
