package com.platzi.profesoresplatzi.dao;

import java.util.List;

import com.platzi.profesoresplatzi.model.Teacher;


public interface TeacherDao {
	
	void saveTeacher(Teacher teacher);

	void deleteTeacherById(Long idTeacher);
	
	void updateTeacher(Teacher teacher);
	
	//Se regresara una lista
	List<Teacher> findAllTeachers();
	
	//Obtener el teacher por su id
	Teacher findById(Long idTeacher);
	
	//Obtener el teacher por su nombre
	Teacher findByName(String name);
}
